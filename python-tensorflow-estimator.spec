%global _empty_manifest_terminate_build 0
Name:		tensorflow-estimator
Version:	2.12.0
Release:	1
Summary:	TensorFlow Estimator.
License:	Apache 2.0
URL:		https://www.tensorflow.org/
Source0:	https://files.pythonhosted.org/packages/d1/e1/c3596da404e2c47561a2bb392397208925e65be6f61bd3081e630371d5e8/tensorflow_estimator-2.12.0-py2.py3-none-any.whl
BuildArch:	noarch

%description
TensorFlow Estimator is a high-level API that encapsulates model training,
evaluation, prediction, and exporting.

%package -n python3-tensorflow-estimator
Summary:	TensorFlow Estimator.
Provides:	python-tensorflow-estimator
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:	python3-six
BuildRequires:	bazel
%description -n python3-tensorflow-estimator
TensorFlow Estimator is a high-level API that encapsulates model training,
evaluation, prediction, and exporting.

%package help
Summary:	Development documents and examples for tensorflow-estimator
Provides:	python3-tensorflow-estimator-doc
%description help
TensorFlow Estimator is a high-level API that encapsulates model training,
evaluation, prediction, and exporting.

%prep

%build
mkdir dist
cp %{SOURCE0} dist

%install
%py3_install_wheel *
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%files -n python3-tensorflow-estimator
%{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Sep 19 2023 yixiangzhike <yixiangzhike007@163.com> - 2.12.0-1
- upgrade to version 2.12.0 for tensorflow 2.12.1 build requirement

* Mon Nov 21 2022 Bin Hu <hubin73@huawei.com> - 2.10.0-1
- downgrade to version 2.10.0 for tensorflow 2.10 build requirement

* Mon Nov 21 2022 Bin Hu <hubin73@huawei.com> - 2.11.0-1
- upgrade to version 2.11.0 for tensorflow 2.10 build

* Thu Oct 08 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
